FROM erlang:slim

RUN apt-get update && apt-get install make git -y

WORKDIR /app
COPY . .
RUN make compile

EXPOSE 8080

CMD make start