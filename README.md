# Задача 1

Dockerfile находится в корне.
Команды для запуска:
```
docker build --pull -t chat
docker run --name chat -p 8080:8080 chat
```

# Задача 2

Описание pipeline в файле .gitlab-ci.yaml. Запускается при каждом пуше в репозиторий, билдит образ и пушит в репозиторий registry.gitlab.com/ilyatrub/rbkmoney

# Задача 3

Helm chart в папке chat-helm. В templates описаны namespace, deployment, configmap, service. В values.yaml проставлены некоторые параметры для шаблонов.

Запуск:
```
helm install chat ./chat-helm
```

Проверял на minikube, из-за прописанного пути в js коде пришлось дополнительно выполнить команду:

```
kubectl port-forward svc/chat-service 8080:8080
```

Приложение будет доступно на localhost:8080 или по ip из команды:
```
minikube service chat-service -n chat-ns
```